package br.ucsal.loja.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.ucsal.loja.dao.CarneDAO;
import br.ucsal.loja.model.Carne;

@WebServlet("/AdicionarCarneServlet")
public class AdicionarCarneServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String nome = request.getParameter("nome_carne");
		String validade = request.getParameter("validade");
		Double valor_quilo = Double.parseDouble(request.getParameter("valor_kilo"));

		Carne carne = new Carne();
		carne.setNome(nome);
	    carne.setValidade(validade);
		carne.setValorQuilo(valor_quilo);
		
		CarneDAO dao = new CarneDAO();
		dao.inserir(carne);

		response.sendRedirect("ListarCarneServlet");

	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
