package br.ucsal.loja.model;

public class Carne {

	private Long id;
	private String nome;
	private Double valorQuilo;
	private String validade;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getValorQuilo() {
		return valorQuilo;
	}

	public void setValorQuilo(Double valorQuilo) {
		this.valorQuilo = valorQuilo;
	}

	public String getValidade() {
		return validade;
	}

	public void setValidade(String validade) {
		this.validade = validade;
	}

}