package br.ucsal.loja.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

	private static ConnectionFactory connectionFactory = null;

	private static Connection connection = null;

	private static final String DRIVER = "org.postgresql.Driver";
	private static final String DBURL = "jdbc:postgresql://localhost:6543/Site_Carne";
	private static final String USER = "postgres";
	private static final String SENHA = "postgresql";

	private ConnectionFactory() {
		try {
			Class.forName(DRIVER);
			connection = DriverManager.getConnection(DBURL, USER, SENHA);
			connection.createStatement()
					.execute("CREATE TABLE IF NOT EXISTS CARNE  ( " + "  ID_CARNE SERIAL PRIMARY KEY,"
							+ "  NOME_CARNE VARCHAR(255), VALOR_QUILO DOUBLE PRECISION, VALIDADE VARCHAR(255)" + ");");
			connection.createStatement()
					.execute("CREATE TABLE IF NOT EXISTS CLIENTE (" + "ID_CLIENTE SERIAL PRIMARY KEY, "
							+ "CPF VARCHAR(11), NOME_CLIENTE VARCHAR(255), END_CLIENTE VARCHAR(255), SENHA_CLIENTE VARCHAR(255) "
							+ ");");

			// Criar a tabela usuario
			// Criar a tabela papel
		} catch (SQLException | ClassNotFoundException e) {
			throw new RuntimeException(e);
		}

	}

	public static Connection getConnection() {
		if (connectionFactory == null) {
			connectionFactory = new ConnectionFactory();
		}
		return connection;
	}

}
