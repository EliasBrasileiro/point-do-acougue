package br.ucsal.loja.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.ucsal.loja.model.Carne;

public class CarneDAO {

	private Connection connection;

	public CarneDAO() {
		this.connection = ConnectionFactory.getConnection();
	}

	public void inserir(Carne carne) {
		String sql = "insert into carne (nome_carne, valor_quilo, validade) values (?,?,?)";
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setString(1, carne.getNome());
			stmt.setDouble(2, carne.getValorQuilo());
			stmt.setString(3, carne.getValidade());
			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	public List<Carne> getLista() {
		try {
			List<Carne> carnes = new ArrayList<>();
			String sql = "select * from carne";
			PreparedStatement stmt = this.connection.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Carne carne = new Carne();
				carne.setId(rs.getLong("id_carne"));
				carne.setNome(rs.getString("nome_carne"));
				carne.setValidade(rs.getString("validade"));
				carne.setValorQuilo(rs.getDouble("valor_quilo"));
				carnes.add(carne);
			}
			rs.close();
			stmt.close();
			return carnes;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public Carne obter(Long id) {
		Carne carne = null;
		String sql = "select (id_carne,nome_carne, validade, valor_quilo) where id_carne=?";
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				carne = new Carne();
				carne.setId(rs.getLong("id_carne"));
				carne.setNome(rs.getString("nome_carne"));
				carne.setValidade(rs.getString("validade"));
				carne.setValorQuilo(rs.getDouble("valor_quilo"));
			}
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

		return carne;

	}

	public void altera(Carne carne) {
		String sql = "update carne set nome_carne=? validade=? valor_quilo=? where id_carne=?";
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setString(1, carne.getNome());
			stmt.setString(2, carne.getValidade());
			stmt.setDouble(3, carne.getValorQuilo());
			stmt.setLong(4, carne.getId());
			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void remove(Carne carne) {
		try {
			PreparedStatement stmt = connection.prepareStatement("delete from carne where id_carne=?");
			stmt.setLong(1, carne.getId());
			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

}
