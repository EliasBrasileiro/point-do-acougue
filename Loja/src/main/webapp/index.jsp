<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<link rel="shortcut icon"
	href="https://img.icons8.com/color/50/000000/steak.png">


<link rel="stylesheet" href="/css/custom.css" />
<link rel="stylesheet" href="/css/loja.css" />
<link rel="stylesheet"
	href="/webjars/bootstrap/4.1.3/css/bootstrap.min.css" />

<title>Point da Carne</title>
</head>
<body>

	<c:import url="topo.jsp"></c:import>

	<main role="main text-dark">

	<section class="jumbotron text-center text-danger ">
		<div class="container">
			<h1 class="jumbotron-heading">Point da Carne</h1>
			<p class="lead text-muted">Melhor açougue da região, venha
				conhecer nossos produtos e nossos preços, qualidade e preço baixo
				fazem parte do nosso dilema. A concorrência aqui passa longe.
				Estamos localizados na Rua Hélio Machado, 1622 - Boca do Rio -
				Salvador-BA.</p>
			<p>
				<a href="./ListarCarneServlet" class="btn btn-primary my-2">Conheça
					nossos cortes</a> <a href="./login.jsp" class="btn btn-secondary my-2">Fazer
					Login</a>
			</p>
		</div>
	</section>

	</main>


	<c:import url="rodape.jsp"></c:import>

	<script src="/webjars/jquery/3.3.1-1/jquery.min.js"></script>
	<script src="/webjars/popper.js/1.14.4/umd/popper.min.js"></script>
	<script src="/webjars/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<script src="/webjars/holderjs/2.5.2/holder.min.js"></script>


</body>
</html>