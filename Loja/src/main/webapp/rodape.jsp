<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<link rel="shortcut icon"
	href="https://img.icons8.com/color/50/000000/steak.png">

<link rel="stylesheet" href="/css/custom.css" />
<link rel="stylesheet" href="/css/loja.css" />
<link rel="stylesheet"
	href="/webjars/bootstrap/4.1.3/css/bootstrap.min.css" />
</head>
<body>
	<footer class="text-muted">
		<div class="container">
			<p class="float-right">
				<a href="#">Voltar para o Topo</a>
			</p>
			<p>&copy; Point da Carne!</p>
		</div>
	</footer>


	<script src="/webjars/jquery/3.3.1-1/jquery.min.js"></script>
	<script src="/webjars/popper.js/1.14.4/umd/popper.min.js"></script>
	<script src="/webjars/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<script src="/webjars/holderjs/2.5.2/holder.min.js"></script>

</body>
</html>