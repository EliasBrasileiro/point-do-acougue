<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Listagem de carnes</title>
	</head>
	<body>
		<table>
			<tr>
				<td>Carnes</td>
			</tr>
			<c:forEach var="carnes" items="${carnes}"> 
				<tr>
					<td>${carnes.nome_carne}</td>
					<td>${carnes.validade}</td>
					<td>${carnes.valor_kilo}</td>
				</tr>
			</c:forEach>
		</table>
		
	</body>
</html>