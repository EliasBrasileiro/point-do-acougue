<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" href="/css/custom.css" />
<link rel="stylesheet" href="/css/loja.css" />
<link rel="stylesheet"
	href="/webjars/bootstrap/4.1.3/css/bootstrap.min.css" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">


<meta charset="ISO-8859-1">
<link rel="shortcut icon"
	href="https://img.icons8.com/color/50/000000/steak.png">
<title>Point da Carne</title>
</head>
<body>

	<c:import url="../topo.jsp"></c:import>
	<div class="album py-5 bg-light">
		<div class="container">

			<div class="row">

				<c:forEach var="carnes" items="${carnes}">

					<div class="col-md-4 divHome">
						<div class="card mb-4 box-shadow">
							<img class="card-img-top" src="../${carnes.nome}	.jpg"
								alt="Card image cap">
							<div class="card-body">
								<p class="card-text">${carnes.nome}</p>
								<div class="d-flex justify-content-between align-items-center">
									<div class="btn-group">
										<button type="button" class="btn btn-sm btn-outline-secondary">Detalhes</button>
										<button type="button" class="btn btn-sm btn-outline-secondary">Comprar</button>
									</div>
									<small class="text-muted">${carnes.valorQuilo}</small>
								</div>
							</div>
						</div>
					</div>

				</c:forEach>


			</div>
		</div>
	</div>
	<br>


	<c:import url="../rodape.jsp"></c:import>
</body>
</html>